#-*- coding: utf-8 -*-
import donnees
import random
import pickle
import os

def initialize_score_player():

  if not os.path.isfile('donnees'):
    with open('donnees', 'wb') as file:
      scores = {}
      my_pickler = pickle.Pickler(file)
      my_pickler.dump(scores)
  

def register_score_player(name, score = 0):

  initialize_score_player()
  add_name = False
  current_scores = None

  with open('donnees', 'rb') as file:
    my_depickler = pickle.Unpickler(file)
    current_scores = my_depickler.load()
    if name not in current_scores.keys():
      add_name = True
  
  if add_name:
    current_scores[name] = 0
    with open('donnees', 'wb') as file:
      my_pickler = pickle.Pickler(file)
      my_pickler.dump(current_scores)
      if score == 0:
        print("\n Vous êtes un nouveau joueur! bienvenue {}!".format(name))
  
  else:
    current_scores[name] += score
    with open('donnees', 'wb') as file:
      my_pickler = pickle.Pickler(file)
      my_pickler.dump(current_scores)
      if score == 0:
        print("\n Heureux de vous revoir {}! Votre score {}.".format(name, score))

def display_score():
  with open('donnees', 'rb') as file:
    my_depickler = pickle.Unpickler(file)
    current_scores = my_depickler.load()
    print('\n')
    print(current_scores)


def random_word():
  index = random.randint(0, len(donnees.WORDS))
  return donnees.WORDS[index].upper()

def game():
  name = str(input("\nEntrer votre pseudo et voyons si vous êtes nouveau!: "))
  while (len(name) > 20 or len(name) < 4) or not name.isidentifier():
    print("\n Votre pseudo doit comporter entre 4 et 20 characters, il peut contenir [a-z][0-9]_, une lettre minimum")
    name = str(input("\n Entrer de nouveau votre pseudo."))
  
  register_score_player(name)
  
  game = True

  while game:
    find_word = random_word()
    list_vanish_word = ['*']*len(find_word)
    vanish_word = "".join(list_vanish_word)
    print('\n')
    print("Le mot à trouver: {}".format(vanish_word).center(50))
    
    count_try = donnees.TRY
    score = 0
    game = False
    while count_try > 0:

      choice = str(input("\nEntrer votre lettre: ")).upper()

      while len(choice) != 1 or not choice.isalpha():
        print('\nVous avez entré un nombre ou plusieurs character.')
        choice = str(input("\nEntrer votre lettre: ")).upper()
      
      if choice in find_word:
        for key, letter in enumerate(find_word):
          if choice == letter:
            list_vanish_word[key] = choice
        count_try += 1

      vanish_word = "".join(list_vanish_word).center(68)
      print(vanish_word)


      if vanish_word.find('*') == -1:
        score = count_try-1
        break

      count_try -= 1
      print("plus que {}".format(count_try))

    if score != 0:
      print("\nFelicitation! vous avez trouvé le mot! Votre score: {}".format(score))
      register_score_player(name, score)
    else:
      print("\nVous n'avez pas trouvé le mot à temps :(")
    
    choice_game = str(input('\n Tapez [Q] pour quitter, [C] table de score, autre pour rejouer !'))
    if choice_game.upper() == 'C':
      display_score()
      choice_game = str(input('\n Tapez [Q] pour quitter, [C] table de score, autre pour rejouer !'))

    if choice_game.upper() != 'Q':
      game = True


def main():
  pass

if __name__ == '__main__':
  main()




